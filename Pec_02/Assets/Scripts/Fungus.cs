using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fungus : MonoBehaviour
{
    private GameObject uiManager;
    private GameObject player;

    // Start is called before the first frame update
    public void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
    }

    private void DestroyFungus()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Heat")
        {
            player.GetComponent<Player2DController>().jumpHeight = 17;
            
            Debug.Log("Tengo mas poder");
            Invoke("DestroyFungus", 0.2f);

        }
    }
}
