using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperCoin : MonoBehaviour
{
    private GameObject uiManager;

    // Start is called before the first frame update
    public void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
    }

    private void DestroySuperCoin()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Heat")
        {
            General.SetMaxNumCoins(General.GetMaxNumCoins() + General.GetNewCoinsB());
            uiManager.GetComponent<UIManager>().SetNumCoinsText();
            Debug.Log("Estoy tomando monedas");
            Invoke("DestroySuperCoin", 0.2f);
        }
    }
}
