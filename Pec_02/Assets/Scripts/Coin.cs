using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private GameObject uiManager;

    // Start is called before the first frame update
    public void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
    }

    private void DestroyCoin()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Heat")
        {
            General.SetMaxNumCoins(General.GetMaxNumCoins() + General.GetNewCoinsA());
            uiManager.GetComponent<UIManager>().SetNumCoinsText();
            Debug.Log("Estoy tomando monedas");
            Invoke("DestroyCoin", 0.2f);
        }
    }
}
