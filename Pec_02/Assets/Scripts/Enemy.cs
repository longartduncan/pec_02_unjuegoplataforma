using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private GameObject uiManager;
    private float velocity;

    public void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
    }
    // Start is called before the first frame update
    void Start()
    {
        velocity = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Translate(-velocity * Time.deltaTime, 0.0f, 0.0f);
    }

    public void SetVelocity(float ve)
    {
        velocity = ve;
    }
    public float GetVelocity()
    {
        return velocity;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag=="Foot")
        {
            General.SetMaxNumRecords(General.GetMaxNumRecords() + General.GetNewRecordsC());
            uiManager.GetComponent<UIManager>().SetNumRecordsText();

            Destroy(this.gameObject);
        }

        if (other.gameObject.tag == "Heat")
        {
            General.SetMaxNumRecords(General.GetMaxNumRecords() + General.GetNewRecordsC());
            uiManager.GetComponent<UIManager>().SetNumRecordsText();

            Destroy(this.gameObject);
        }
    }

}
