using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2DController : MonoBehaviour
{
    private GameObject player;
    
    private float move;
    private float jump;
    public float playerSpeed;
    public float jumpHeight = 10;
    public float jumpDown = -16;

    private bool isJump;
    private bool isDead;

    private GameObject uiManager;

    // Start is called before the first frame update
    private void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
    }

    void Start()
    {
        isJump = false;
        isDead = false;
    }

    // Update is called once per frame
    void Update()
    {       
        if (!isDead)
        {
            move = Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime;
            jump = Input.GetAxis("Jump");
            this.gameObject.transform.Translate(move, 0.0f, 0.0f);
            
            if (move > 0.0f)
            {
                this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX = false;
                this.gameObject.transform.GetChild(0).GetComponent<Animator>().SetInteger("StatePlayer", 1);
                Debug.Log("Me estoy moviendo");
            }
            else if (move < 0.0f)
            {
                this.gameObject.transform.GetChild(0).GetComponent<Animator>().SetInteger("StatePlayer", 1);
                this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                if (!isJump)
                {
                    this.gameObject.transform.GetChild(0).GetComponent<Animator>().SetInteger("StatePlayer", 0);
                }
            }
        }
        

        if (!isJump)
        {
            
            if (jump > 0 && this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipY == false)
            {
                this.gameObject.transform.GetChild(0).GetComponent<Animator>().SetInteger("StatePlayer", 2);
                this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 3;
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, jumpHeight);
                Debug.Log("Estoy saltando");
                isJump = true;
            }
            else if (jump > 0 && this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipY == true)
            {
                this.gameObject.GetComponent<Rigidbody2D>().gravityScale = -3;
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, jumpDown);
                Debug.Log("Estoy saltando al reves");
                isJump = true;
            }


        }
        
    }

    private void IsWinNow()
    {
        uiManager.GetComponent<UIManager>().ShowPanelWin();
    }

    private void IsDeadNow()
    {
        isDead = true;
        uiManager.GetComponent<UIManager>().ShowPanelOver();
        this.gameObject.transform.GetChild(0).GetComponent<Animator>().SetInteger("StatePlayer", 3);

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        

        if (other.gameObject.tag == "Blocks")
        {
            
            isJump = false;
        }

        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("Me mata");
            //isJump = false;
            IsDeadNow();
        }

        if (other.gameObject.tag == "Platforms")
        {
            this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 3;
            this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipY = false;
            isJump = false;
        }

        if (other.gameObject.tag == "AntiPlatforms")
        {
            this.gameObject.GetComponent<Rigidbody2D>().gravityScale = -3;
            this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipY = true;
            isJump = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Vacuum")
        {
            Debug.Log("Me caigo");
            IsDeadNow();
        }

        if (other.gameObject.tag == "Finish")
        {
            Debug.Log("Gane");
            IsWinNow();
        }

        

    }
}
