using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    private GameObject uiManager;

    // Start is called before the first frame update
    public void Awake()
    {
        uiManager = (GameObject)GameObject.FindGameObjectWithTag("UIManager");
    }

    private void DestroyBlock()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Heat")
        {
            
            General.SetMaxNumRecords(General.GetMaxNumRecords() + General.GetNewRecordsA());
            uiManager.GetComponent<UIManager>().SetNumRecordsText();
            Debug.Log("Estoy rompiendo el cubo");
            Invoke("DestroyBlock", 0.2f);
        }
    }
}
