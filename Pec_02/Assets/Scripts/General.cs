using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class General
{
    private static int minNumCoins = 00;
    private static int maxNumCoins = 99;

    private static int minNumRecords = 000000;
    private static int maxNumRecords = 999999;

    private static int numBlocks = 00;

    private static int newCoinsA = 1;
    private static int newCoinsB = 5;
    private static int newRecordsA = 10;
    private static int newRecordsB = 50;
    private static int newRecordsC = 100;


    public static int GetMinNumCoins()
    {
        return minNumCoins;
    }
    public static void SetMinNumCoins(int mNC)
    {
        minNumCoins = mNC;
    }

    public static int GetMaxNumCoins()
    {
        return maxNumCoins;
    }
    public static void SetMaxNumCoins(int nC)
    {
        maxNumCoins = nC;
    }

    public static int GetMinNumRecords()
    {
        return minNumRecords;
    }
    public static void SetMinNumRecords(int mNR)
    {
        minNumRecords = mNR;
    }
    public static int GetMaxNumRecords()
    {
        return maxNumRecords;
    }
    public static void SetMaxNumRecords(int nR)
    {
        maxNumRecords = nR;
    }

    public static int GetNumBlocks()
    {
        return numBlocks;
    }
    public static void SetNumBlocks(int nB)
    {
        numBlocks = nB;
    }

    public static int GetNewCoinsA()
    {
        return newCoinsA;
    }
    public static void SetNewCoinsA(int nCA)
    {
        newCoinsA = nCA;
    }

    public static int GetNewCoinsB()
    {
        return newCoinsB;
    }
    public static void SetNewCoinsB(int nCB)
    {
        newCoinsB = nCB;
    }

    public static int GetNewRecordsA()
    {
        return newRecordsA;
    }
    public static void SetNewRecordsA(int nRA)
    {
        newRecordsA = nRA;
    }
    public static int GetNewRecordsB()
    {
        return newRecordsB;
    }
    public static void SetNewRecordsB(int nRB)
    {
        newRecordsA = nRB;
    }
    public static int GetNewRecordsC()
    {
        return newRecordsC;
    }
    public static void SetNewRecordsC(int nRC)
    {
        newRecordsA = nRC;
    }

}
