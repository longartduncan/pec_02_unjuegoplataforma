using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private GameObject panelPause;
    private GameObject panelWin;
    private GameObject panelOver;

    private GameObject namePlayer;
    
    private GameObject numCoinsText;
    private GameObject numRecordsText;
    private GameObject timeRun;

    private int statePause;
    private int statePanelPause;

    private void Awake()
    {
        panelPause = (GameObject)GameObject.FindGameObjectWithTag("PanelPause");
        panelWin = (GameObject)GameObject.FindGameObjectWithTag("PanelWin");
        panelOver = (GameObject)GameObject.FindGameObjectWithTag("PanelOver");

        numCoinsText = (GameObject)GameObject.FindGameObjectWithTag("NumCoins");
        numRecordsText = (GameObject)GameObject.FindGameObjectWithTag("NumRecords");

        statePanelPause = 0;

        NoShowPanel();

        General.SetMaxNumCoins(General.GetMinNumCoins());
        General.SetMaxNumRecords(General.GetMinNumRecords());

        //SetNumCoinsText();
        //SetNumRecordsText();
    }
    // Start is called before the first frame update
    void Start()
    {
        SetNumCoinsText();
        SetNumRecordsText();
    }

    public void SetNumCoinsText()
    {
        if (numCoinsText != null) numCoinsText.GetComponent<Text>().text = General.GetMaxNumCoins().ToString();
    }

    public void SetNumRecordsText()
    {
        if (numRecordsText != null) numRecordsText.GetComponent<Text>().text = General.GetMaxNumRecords().ToString();
    }

    public void ShowPanelWin()
    {
        if (panelWin)
        {
            panelWin.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    public void ShowPanelOver()
    {
        if (panelOver)
        {
            panelOver.SetActive(true);
            //Time.timeScale = 0.0f;
        }
    }

    public void ShowPanelPause(int state)
    {
        if (!panelPause.activeSelf)
        {
            panelPause.SetActive(true);
        }
        if (state == 1)
        {
            Debug.Log("Esta en Pausa");
        }
        else if (state == 2)
        {
            Debug.Log("Esta en Pausa 2");
        }
        else
        {
            Time.timeScale = 1.0f;
            Debug.Log("Esta pausa 3");
        }
    }

    public void NoShowPanel()
    {
        if (panelPause!=null)
        {
            panelPause.SetActive(false);
        }
        if (panelWin!=null)
        {
            panelWin.SetActive(false);
        }
        if (panelOver!=null)
        {
            panelOver.SetActive(false);
        }
    }

    //Main Menu
    public void Play()
    {
        SceneManager.LoadScene("Level_1");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void Exit()
    {
        Application.Quit();
    }

    //Panel Over

    public void Again()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    //Panel Pause
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (statePanelPause==0)
            {
                ShowPanelPause(statePanelPause);
                statePanelPause = 1;
                Time.timeScale = 0.0f;
            }
            else
            {
                NoShowPanel();
                statePanelPause = 0;
                Time.timeScale = 1.0f;
            }
            
            
        }
    }


}
